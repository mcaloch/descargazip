import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DescargaService {
  instancia="http://localhost/proyectos/";
  prefijo = "pruebas/zipfile.php";

  private archivoUrl = this.instancia+this.prefijo+'/localidad/index';

  constructor(
    private http: HttpClient
  ) { }


  descarga2(archivo:any): Observable<any> {
    return this.http.post(this.archivoUrl,JSON.stringify(archivo) ,{'responseType': 'arraybuffer'});
  }

}
