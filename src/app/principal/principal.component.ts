import { Component, OnInit } from '@angular/core';
import { DescargaService } from '../descarga.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {
  texto:string;
  constructor(
    private descargaService: DescargaService
    ) { }

  ngOnInit(): void {
    this.texto = "World 899";
  }

   obtenerArchivo(){
     //Esta opcion requiere pestana o popup habilitada por el usuario
    let valor = {};
    console.log("Inicia descarga");
    this.descargaService.descarga2(valor).subscribe((data) =>{
      const blob = new Blob([data],{type:'application/zip'});
      var descarga = window.URL.createObjectURL(blob);
      window.open(descarga);
      console.log("Termina descarga");
    },
    error => {
      console.log(error);
    });
   }

   getArchivo(){
    let valor = {};
    console.log("Inicia descarga");
    this.descargaService.descarga2(valor).subscribe((data) =>{
      const blob = new Blob([data],{type:'application/zip'});

      const a: any = document.createElement('a');
      document.body.appendChild(a);

      var descarga = window.URL.createObjectURL(blob);
      a.href = descarga;
      a.download = "sua_"+Date()+".zip";
      a.click();
      window.URL.revokeObjectURL(descarga);
      console.log("Termina descarga");
    },
    error => {
      console.log(error);
    });
   }

}
