export interface ZipFile{
    id: number;
    tipo: number;
    idEmpresa: number;
    fechaDel: string;
    fechaAl: string;
}